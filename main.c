#include "stm32f4xx_hal.h"
#include "delay.h"
#include "usart.h"
#include "usart_unit_test.h"
#include "cli.h"
#include "string.h"
#include "FreeRTOS.h"
#include "task.h"
#include "trace.h"
#include "semphr.h"

void LED_Init (void);
void delay(void);
void delay2(int vdelay);
void taskLED(void* params);
void taskLED2(void* params);
void commandLED(char *param);
void commandLED2(char *param);
void taskCLI(void* params);
void taskLED(void* params);
void activity(void);
void taskDELAY(void* params);
void SystemClock_Config(void);
void taskSemaphor(void* params);
void EXTI2_IRQHandler(void);
static void EXTI2_Init(void);
void taskHELLO(void* params);
void USART_POLL_WriteString(const char *string);


void taskTXQ(void *params);
void taskRXQ(void *params);

extern volatile uint16_t ticks;

typedef struct {
        GPIO_TypeDef* gpio;    // GPIO port
        uint16_t      pin;     // GPIO pin
        TickType_t    ticks;   // delay expressed in system ticks
} BlinkParams;


int main (void) {
	
	//kolejne dwa ezgemplarze struktury:, dobrze ze sa static
	static BlinkParams bp1 = { .gpio = GPIOG, .pin = GPIO_PIN_13, .ticks = 500};
	static BlinkParams bp2 = { .gpio = GPIOG, .pin = GPIO_PIN_14, .ticks = 1000};
	static BlinkParams bp3 = { .gpio = GPIOG, .pin = GPIO_PIN_15, .ticks = 1000};
	//static BlinkParams bpsem = { .gpio = GPIOG, .pin = GPIO_PIN_14, .ticks = 1000};

  HAL_Init();
	SystemClock_Config(); //zwiekszenie dokladnosci zegara
	LED_Init();
	__GPIOG_CLK_ENABLE();
	TRACE_Init(); //implementacja trace

  USART_Init();
	EXTI2_Init();

	static TaskHandle_t taskHandle;
	//create FreeRTOS task and save it's handle in taskHandle
	if (pdPASS != xTaskCreate(taskLED, "led1", configMINIMAL_STACK_SIZE, &bp1, 3, &taskHandle)) {
	printf("ERROR: Unable to create task!\n");
	}
	// bind the newly created task with trace signal 1
	TRACE_BindTaskWithTrace(taskHandle, 1);
	if (pdPASS != xTaskCreate(taskLED, "led1", configMINIMAL_STACK_SIZE, &bp2, 3, &taskHandle)) {
	printf("ERROR: Unable to create task!\n");
	}
	TRACE_BindTaskWithTrace(taskHandle, 2);
	//bind the newly created task with trace signal 1
	if (pdPASS != xTaskCreate(taskCLI, "led1", configMINIMAL_STACK_SIZE, &bp3, 3, &taskHandle)) {
	printf("ERROR: Unable to create task!\n");
	}
	TRACE_BindTaskWithTrace(taskHandle, 3);

	
	//at the end of the tasks
	vTaskStartScheduler();
	
}

void LED_Init (void) {
	GPIO_InitTypeDef GPIO_InitStruct;
	
  __GPIOG_CLK_ENABLE();
	  // GPIO Ports Clock Enable
	
	  // Configure GPIO pin PG13
  GPIO_InitStruct.Pin   = GPIO_PIN_13;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;        
  GPIO_InitStruct.Pull  = GPIO_PULLDOWN;              
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;           
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	
	  // Configure GPIO pin PG14
	GPIO_InitStruct.Pin   = GPIO_PIN_14;           
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	
}

//Command's for switching leds 

void commandLED(char *param) {
	if (!strcmp(param, " on")) 
		{
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_SET);
		USART_WriteString("Turning on LED");
		USART_WriteString("\n\r");
	} else if (!strcmp(param, " off")) 
	{
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);
		USART_WriteString("Turning off LED2");
		USART_WriteString("\n\r");
	} else 
	{
		USART_WriteString("Unrecognized argument for led. Available arguments are \"on\" and \"off\"\n\r");
		USART_WriteString("\n\r");

	}
}

void commandLED2(char *param) {
	if (!strcmp(param, " on")) 
		{
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
			USART_WriteString("Turning on LED2");
		USART_WriteString("\n\r");
	} else if (!strcmp(param, " off")) 
	{
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);
		USART_WriteString("Turning off LED2");
		USART_WriteString("\n\r");
	} else {
		USART_WriteString("Unrecognized argument for led2. Available arguments are \"on\" and \"off\"\n\r");
		USART_WriteString("\n\r");
	}
}


void taskLED2(void* params)
{
    // Toggle the LED on pin PG.13
    while (1) {
        // toggle the LED
        HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);
        // introduce some delay
        vTaskDelay(1000);
    } // while (1)
} /* taskLED */

void taskCLI(void* params)
{
	
	CLI_CommandItem wrong_item = { .callback = NULL,
                            .commandName = "WrongItemThisShouldn'tBeAdded",
                           .description = NULL};
        
	static CLI_CommandItem item_LED = { .callback = commandLED,
                          .commandName = "LED",
                          .description = NULL};
	
	static CLI_CommandItem item_LED2 = { .callback = commandLED2,
                          .commandName = "LED2",
                          .description = NULL};
 
	if(CLI_AddCommand(&item_LED) == false){
			USART_WriteString("ERROR in adding new item.\n\r");
	}
	if(CLI_AddCommand(&item_LED2) == false){
			USART_WriteString("ERROR in adding new item.\n\r");
	}
	CLI_PrintAllCommands();
	
	while(1)
	{
		// introduce some delay
		//Toggle CLI function
		CLI_Proc();
		vTaskDelay(10);
	}
} /* taskLED */

void taskLED(void* params)
{
  // Blink the LED based on the provided arguments
  while (params) {
    // Toggle LED
    HAL_GPIO_TogglePin(((BlinkParams*)params)->gpio, ((BlinkParams*)params)->pin);
    // Introduce some delay
    vTaskDelay(((BlinkParams*)params)->ticks);
  } // while (params)
 
  // Delete the task in case the provided argument is NULL
  vTaskDelete(NULL);
 
} /* taskLED */


void activity(void)
{
    static volatile uint32_t period = 400000;
    volatile uint32_t i;
        
    // this loop executes 400000 or 80000 times,
    // which makes this activity once longer once shorter in time
    for (i = 0; i < period; i++) {
        __asm__ volatile ("NOP");
    }
        
    if (period == 400000) {
        period = 80000;
    } else {
        period = 400000;
    }
}
 
void taskDELAY(void* params)
{
    TickType_t start, stop;
        
    // we are going to use these two trace signals: 
    TRACE_SetLow(5);
    TRACE_SetLow(6);
        
    while (1) {         
        // trace signal 5 toggles on every loop
        TRACE_Toggle(5);
        // save activity start time
        start   = xTaskGetTickCount();
        // trace 6 goes high at the start of activity
        TRACE_SetHigh(6);
        activity();
        // trace 6 goes low at the end of activity
        TRACE_SetLow(6);
        // save activity end time
        stop    = xTaskGetTickCount();
        // delay task execution until 500ms - duration of activity
        vTaskDelay(500 - (stop-start));
    }
}

void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
 
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
 
  /* The voltage scaling allows optimizing the power consumption when the
     device is clocked below the maximum system frequency (see datasheet). */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
 
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
 
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 |
                                RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);
}


/**
 * Configures EXTI Line2 (connected to PG2 pin) in interrupt mode
 */
static void EXTI2_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
        
  // Enable GPIOG clock
  __GPIOG_CLK_ENABLE();
  
  // Configure PG2 pin as input with EXTI interrupt on the falling edge and pull-up
  GPIO_InitStructure.Speed = GPIO_SPEED_LOW;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = GPIO_PIN_2;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStructure);
 
        
  // Enable and set EXTI Line2 Interrupt to the lowest priority
  HAL_NVIC_SetPriority(EXTI2_IRQn, 15, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);
}
 
/**
 * This function handles External line 2 interrupt request.
 */
void EXTI2_IRQHandler(void)
{
  // Check if EXTI line interrupt was detected
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_2) != RESET)  {
    // Clear the interrupt (has to be done for EXTI)
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_2);
    // Toggle LED
    //HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);
		HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_13);
		xSemaphoreGiveFromISR(mySemaphore, NULL);
  }
}
