/* Includes ------------------------------------------------------------------*/
#include <assert.h>
#include "ring_buffer.h"


bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) 
{
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
	    ringBuffer->dataBuffer = dataBuffer;
		ringBuffer->dataBufferSize = dataBufferSize;
		ringBuffer->head = ringBuffer->tail= 0;
		ringBuffer->count = 0;
		return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
	        ringBuffer->count = 0;
			ringBuffer->head =0;
			ringBuffer->tail= 0;	
		return true;
	}
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer)
{
  assert(ringBuffer);	
	if(ringBuffer->count==0){
	    return true;
	} else {
	    return false;
	}
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->count;
	}
	return 0;
	
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return ringBuffer->dataBufferSize;
	}
	return 0;	
}

bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c)
{
	assert(ringBuffer);
	
	if ((ringBuffer) && (RingBuffer_GetLen(ringBuffer) < RingBuffer_GetCapacity(ringBuffer))) {
	    
	    ringBuffer->dataBuffer[ringBuffer->head] = c;
			ringBuffer->head++;
			ringBuffer->count = ringBuffer->count + 1; 
		
		if (ringBuffer->head >= ringBuffer->dataBufferSize) {
				ringBuffer->head = 0; 
		}
		return true;
	}
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c)
{
	assert(ringBuffer);
	assert(c);
	
 if ((ringBuffer) && (c) && ringBuffer->count > 0) {
       
		*c = ringBuffer->dataBuffer[(ringBuffer->tail)];
		 ringBuffer->tail ++;
		ringBuffer->count --; 	
	  if (ringBuffer->tail >= ringBuffer->dataBufferSize) {
			ringBuffer->tail = 0; 
		}		
		return true;
	}
	return false;
//	assert(ringBuffer);
//	assert(c);
//	
//  if ((ringBuffer) && (c)) {
//        if(ringBuffer->count==0)							//sprawdzanie underflow
//            return false;
//            
//		*c = ringBuffer->dataBuffer[ringBuffer->tail];			//zapisanie do zmiennej danej z buffora
//		
//	    ringBuffer->tail++;									//inkrementacja tail
//	    if (ringBuffer->tail >= ringBuffer->dataBufferSize)			//ale gdy wychodzi poza size jest zerowany
//	        ringBuffer->tail = 0;
//        
//        ringBuffer->count--;								//dekrementacja length bo zabieramy dana
//        return true;
//	}
//	return false;
}
